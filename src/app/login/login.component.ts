import { Component, OnInit } from '@angular/core';
import { toast } from 'angular2-materialize';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var jQuery:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formData:any;
  resp:any;
  loading=false;
  loader=false;
  formInfo:any;
  response:any;
  formCap:any;
  submitting=false; 
  formTeam:any;
  accountName:any;


  constructor(private route: ActivatedRoute, private router: Router, private api_service:ApiServiceService, private storage_service: StorageServiceService) { 
      if (!this.formData) {
        this.formData = {
          cif:'',
          teamName:'',
          capName:''
        }
        
      }

       if (!this.formInfo) {
        this.formInfo = {
          accountNumber:''
        }
        
      }

      if (!this.formCap) {
        this.formCap = {
            hostHeaderInfo:{
            requestId:"1",
            ipAddress:"127.0.0.1",
            sourceChannelId:"WEB"
          },
           cif:"",
           firstName:"",
           lastName:"",
           position:"",
           teamName:"",
           createdBy:""
        }
        
      }

       if (!this.formTeam) {
        this.formTeam = {
            hostHeaderInfo:{
            requestId:"1",
            ipAddress:"127.0.0.1",
            sourceChannelId:"WEB"
          },
           teamName:"",
           createdBy:""
        }
        
      }
    
  }

  ngOnInit() {

    var key = 'userObj';
    this.storage_service.clearInfo(key);

    // Set the date we're counting down to
    var countDownDate = new Date("Jun 21, 2019 20:00:00").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get todays date and time
      var now = new Date().getTime();
        
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
        
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
      
      var day = days.toString();
      var n = day.length;
      if(n==2){
        var d1 = day.substring(0, 1);
        var d2 = day.substring(1, 2);
          document.getElementById("d1").innerHTML = d1;
          document.getElementById("d2").innerHTML = d2;
      }

      else if(n<2){

        var d1 = "0";
        var d2 = day.substring(0, 1);
          document.getElementById("d1").innerHTML = d1;
          document.getElementById("d2").innerHTML = d2;
      }

      var hour = hours.toString();
      var n1 = hour.length;
      if(n1==2){
        var h1 = hour.substring(0, 1);
        var h2 = hour.substring(1, 2);
          document.getElementById("h1").innerHTML = h1;
          document.getElementById("h2").innerHTML = h2;
      }

      else if(n1<2){

        var h1 = "0";
        var h2 = hour.substring(0, 1);
          document.getElementById("h1").innerHTML = h1;
          document.getElementById("h2").innerHTML = h2;
      }

      var minute = minutes.toString();
      var n2 = minute.length;
      if(n2==2){
        var m1 = minute.substring(0, 1);
        var m2 = minute.substring(1, 2);
          document.getElementById("m1").innerHTML = m1;
          document.getElementById("m2").innerHTML = m2;
      }

      else if(n2<2){

        var m1 = "0";
        var m2 = minute.substring(0, 1);
          document.getElementById("m1").innerHTML = m1;
          document.getElementById("m2").innerHTML = m2;
      }
      
      var second = seconds.toString();
      var n3 = second.length;
      if(n3==2){
        var s1 = second.substring(0, 1);
        var s2 = second.substring(1, 2);
          document.getElementById("s1").innerHTML = s1;
          document.getElementById("s2").innerHTML = s2;
      }

      else if(n3<2){

        var s1 = "0";
        var s2 = second.substring(0, 1);
          document.getElementById("s1").innerHTML = s1;
          document.getElementById("s2").innerHTML = s2;
      }
      


        
      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        
      }
    }, 1000);
  }

  ngAfterViewInit(){
    var self = this;

    if(jQuery.ready){

      jQuery('.accFormatter').on("keypress", function(event){
          if((event.which != 8) || (event.which != 46)){
            var accFirstChar = self.formInfo.accountNumber.substring(0, 1);
            if((accFirstChar=='9')||(accFirstChar=='0')){
    
              if(self.formInfo.accountNumber.length==2){
                var acc1 = self.formInfo.accountNumber
                self.formInfo.accountNumber = acc1 + " ";
              
              }
          
              else if(self.formInfo.accountNumber.length==7){
                var accNum2 = self.formInfo.accountNumber;
                var acc1 = accNum2.substring(0, 2);
                var acc2 = accNum2.substring(3, 7);
                self.formInfo.accountNumber = acc1 + " " + acc2 + " ";
                
              }
          
              else if(self.formInfo.accountNumber.length==11){
                var accNum2 = self.formInfo.accountNumber;
                  var acc1 = accNum2.substring(0, 2);
                  var acc2 = accNum2.substring(3, 7);
                  var acc3 = accNum2.substring(8, 11);
                  self.formInfo.accountNumber = acc1 + " " + acc2 + " " + acc3 + " ";
              }

              else if((event.which == 8) || (event.which == 46)){
                alert("giu");
              }

 
    
            }
          }
      });
    }
  }

  getInfo(){
    this.loading = true;
    var spaceAcc = this.formInfo.accountNumber;
    var accNo = spaceAcc.replace(/ /g, "");
    this.formInfo.accountNumber = accNo;

    var info = JSON.stringify(this.formInfo);
    this.api_service.acc_info(info).subscribe(res => {
      this.response = res;
       if (this.response.hostHeaderInfo.responseCode == "000") {
          this.loading = false;
          this.formCap.firstName = this.response.customerInfo.firstName;
          this.formCap.lastName = this.response.customerInfo.lastName;
          this.formCap.cif = this.response.accountsInfo[0].cif;
          this.formCap.position = "Captain";
          this.formCap.createdBy = this.response.accountsInfo[0].cif;
          this.accountName = this.response.customerInfo.firstName +" " + this.response.customerInfo.lastName ;


        jQuery('.modal').modal(
          {
            dismissible: true,
            opacity: .5,
            inDuration: 300,
            outDuration: 200,
            startingTop: '1%',
            endingTop: '10%',
          }
        );
        jQuery('.accountModal').modal('open');
          
        }
        else{
          this.loading = false;
          toast("Invalid Account Number",3000);
        }
      }, error => {
          this.loading = false;
          console.log(error);
          toast('Oops. Please try again later',2000);
      });


  }

  signin(){
    this.router.navigate(['/user']);
  }

  

  Addcap(){
    this.loader = true;
    if(this.formCap.teamName===''){
      toast("Please Enter Team Name",2000);
    }
    else{
      var info = JSON.stringify(this.formCap);

      this.formData.cif = this.formCap.cif;
      this.formData.capName = this.accountName;
      this.formData.teamName = this.formCap.teamName;

      this.formTeam.teamName = this.formCap.teamName;
      this.formTeam.createdBy = this.formCap.cif;


        var data = JSON.stringify(this.formTeam);
        this.api_service.afcon_team(data).subscribe(res => {
        this.resp = res;
         if (this.resp.hostHeaderInfo.responseCode == "000") {
            this.api_service.afcon_mem(info).subscribe(res => {
            this.resp = res;
             if (this.resp.hostHeaderInfo.responseCode == "000") {
                toast("submitted successfully",3000);
                this.router.navigate(['/profile']);
                jQuery('.accountModal').modal('close');
                this.storage_service.saveInfo('userObj', JSON.stringify(this.formData));
              }
              else{
                console.log(this.resp);
                this.loader = false;
                toast("Couldn't create your team",3000);
              }
            }, error => {
                console.log(error);
                this.loader = false;
                  toast('Oops. Please try again later',3000);
            });
          }
          else{
            console.log(this.resp);
            this.loader = false;
            toast("Couldnt create team. User Already exists in a team",3000);
          }
        }, error => {
            console.log(error);
            this.loader = false;
              toast('Oops. Please try again later',3000);
        });

      


      
    }
  
  }


  checkData1(){
    var accFirstChar = this.formInfo.accountNumber.substring(0, 1);
    if((accFirstChar=='9')||(accFirstChar=='0')){

       if(this.formInfo.accountNumber.length==13){
        var accNum2 = this.formInfo.accountNumber;
          var acc1 = accNum2.substring(0, 2);
          var acc2 = accNum2.substring(2, 6);
          var acc3 = accNum2.substring(6, 9);
          var acc4 = accNum2.substring(9, 13);
          this.formInfo.accountNumber = acc1 + " " + acc2 + " " + acc3 + " "+ acc4;
      }
  

    }

    else{
      this.formInfo.accountNumber = this.formInfo.accountNumber;
    }
    
    
  }

  
  

}
