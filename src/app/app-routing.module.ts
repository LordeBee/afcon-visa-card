import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { ViewComponent } from './view/view.component';
import { DoneComponent } from './done/done.component';
import { UserComponent } from './user/user.component';

import { from } from 'rxjs';

const routes: Routes = [
    { path: "login", component: LoginComponent },
    { path: "profile", component: ProfileComponent },
    { path: "user", component: UserComponent },
    { path: "done", component: DoneComponent },
    { path: "view", component: ViewComponent },
    { path: "", redirectTo: "/login", pathMatch: "full" }
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
