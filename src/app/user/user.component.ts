import { Component, OnInit } from '@angular/core';
import { toast } from 'angular2-materialize';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var jQuery:any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  formInfo:any;
  resp:any;
  loading=false;
  userobj:any;

  constructor(private route: ActivatedRoute, private router: Router, private api_service:ApiServiceService, private storage_service: StorageServiceService) { 
    if (!this.formInfo) {
      this.formInfo = {
        accountNumber:''
      }
      
    }

    if (!this.userobj) {
      this.userobj = {
        staffNo:''
      }
      
    }
    
  }

  ngOnInit() {

    // Set the date we're counting down to
    var countDownDate = new Date("Jun 21, 2019 15:37:25").getTime();

    // Update the count down every 1 second
    var x = setInterval(function() {

      // Get todays date and time
      var now = new Date().getTime();
        
      // Find the distance between now and the count down date
      var distance = countDownDate - now;
        
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
      
      var day = days.toString();
      var n = day.length;
      if(n==2){
        var d1 = day.substring(0, 1);
        var d2 = day.substring(1, 2);
          document.getElementById("d1").innerHTML = d1;
          document.getElementById("d2").innerHTML = d2;
      }

      else if(n<2){

        var d1 = "0";
        var d2 = day.substring(0, 1);
          document.getElementById("d1").innerHTML = d1;
          document.getElementById("d2").innerHTML = d2;
      }

      var hour = hours.toString();
      var n1 = hour.length;
      if(n1==2){
        var h1 = hour.substring(0, 1);
        var h2 = hour.substring(1, 2);
          document.getElementById("h1").innerHTML = h1;
          document.getElementById("h2").innerHTML = h2;
      }

      else if(n1<2){

        var h1 = "0";
        var h2 = hour.substring(0, 1);
          document.getElementById("h1").innerHTML = h1;
          document.getElementById("h2").innerHTML = h2;
      }

      var minute = minutes.toString();
      var n2 = minute.length;
      if(n2==2){
        var m1 = minute.substring(0, 1);
        var m2 = minute.substring(1, 2);
          document.getElementById("m1").innerHTML = m1;
          document.getElementById("m2").innerHTML = m2;
      }

      else if(n2<2){

        var m1 = "0";
        var m2 = minute.substring(0, 1);
          document.getElementById("m1").innerHTML = m1;
          document.getElementById("m2").innerHTML = m2;
      }
      
      var second = seconds.toString();
      var n3 = second.length;
      if(n3==2){
        var s1 = second.substring(0, 1);
        var s2 = second.substring(1, 2);
          document.getElementById("s1").innerHTML = s1;
          document.getElementById("s2").innerHTML = s2;
      }

      else if(n3<2){

        var s1 = "0";
        var s2 = second.substring(0, 1);
          document.getElementById("s1").innerHTML = s1;
          document.getElementById("s2").innerHTML = s2;
      }
      


        
      // If the count down is over, write some text 
      if (distance < 0) {
        clearInterval(x);
        
      }
    }, 1000);
  }

  ngAfterViewInit(){
    var self = this;

    if(jQuery.ready){

      jQuery('.accFormatter').on("keypress", function(event){
          if((event.which != 8) || (event.which != 46)){
            var accFirstChar = self.formInfo.accountNumber.substring(0, 1);
            if((accFirstChar=='9')||(accFirstChar=='0')){
    
              if(self.formInfo.accountNumber.length==2){
                var acc1 = self.formInfo.accountNumber
                self.formInfo.accountNumber = acc1 + " ";
              
              }
          
              else if(self.formInfo.accountNumber.length==7){
                var accNum2 = self.formInfo.accountNumber;
                var acc1 = accNum2.substring(0, 2);
                var acc2 = accNum2.substring(3, 7);
                self.formInfo.accountNumber = acc1 + " " + acc2 + " ";
                
              }
          
              else if(self.formInfo.accountNumber.length==11){
                var accNum2 = self.formInfo.accountNumber;
                  var acc1 = accNum2.substring(0, 2);
                  var acc2 = accNum2.substring(3, 7);
                  var acc3 = accNum2.substring(8, 11);
                  self.formInfo.accountNumber = acc1 + " " + acc2 + " " + acc3 + " ";
              }

              else if((event.which == 8) || (event.which == 46)){
                alert("giu");
              }

 
    
            }
          }
      });
    }
  }

  signup(){
    this.router.navigate(['/login']);
  }


  getInfo(){
    this.loading = true;
    var spaceAcc = this.formInfo.accountNumber;
    var accNo = spaceAcc.replace(/ /g, "");
    this.formInfo.accountNumber = accNo;
    var info = JSON.stringify(this.formInfo);
    this.api_service.acc_info(info).subscribe(res => {
      this.resp = res;
       if (this.resp.hostHeaderInfo.responseCode == "000") {
        this.loading = false;
        this.userobj.staffNo = this.resp.accountsInfo[0].cif;
        var data = JSON.stringify(this.userobj);

        this.api_service.get_members(data).subscribe(res => {
          this.resp = res;
           if (this.resp.hostHeaderInfo.responseCode == "000") {
            this.storage_service.saveInfo('infoObj', JSON.stringify(this.userobj));
             this.router.navigate(['/view']);
           }
           else{
             toast('You have not created a team yet',3000);
           }
         }, error => {
                 console.log(error);
                 toast('Oops. Please try again later',3000);
           });
        
        }
        else{
          console.log(this.resp);
          this.loading = false;
          toast("Invalid Account number",2000);
        }
      }, error => {
          this.loading = false;
          toast('Oops. Please try again later',2000);
      });


  }

  checkData1(){
    var accFirstChar = this.formInfo.accountNumber.substring(0, 1);
    if((accFirstChar=='9')||(accFirstChar=='0')){

       if(this.formInfo.accountNumber.length==13){
        var accNum2 = this.formInfo.accountNumber;
          var acc1 = accNum2.substring(0, 2);
          var acc2 = accNum2.substring(2, 6);
          var acc3 = accNum2.substring(6, 9);
          var acc4 = accNum2.substring(9, 13);
          this.formInfo.accountNumber = acc1 + " " + acc2 + " " + acc3 + " "+ acc4;
      }
  

    }

    else{
      this.formInfo.accountNumber = this.formInfo.accountNumber;
    }
    
    
  }
}
