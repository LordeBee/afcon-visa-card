import { Component, OnInit } from '@angular/core';
import { toast } from 'angular2-materialize';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var $:any;
declare var jQuery:any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	modalID:any;
	formData:any;
	formInfo:any;
	resp:any;
  response:any;
	details=false;
	accountName:any;
  duser = false;
  addsign = false;
  duser2 = false;
  duser3 = false;
  duser4 = false;
	team=false;
	teamN:any;
	last:any;
	username:any;
	loader=false;
	loading=false;
  captainName:any;
  teamName:any;
  createdBy:any;
  formCap:any;
  user:any;
  user1:any;
  user2:any;
  user3:any;
  user4:any;
  user1CIF:any;
  user2CIF:any;
  user3CIF:any;
  user4CIF:any;
  userobj:any;




  constructor(private route: ActivatedRoute, private router: Router, private api_service:ApiServiceService, private storage_service: StorageServiceService) {
     if (!this.formCap) {
        this.formCap = {
            hostHeaderInfo:{
            requestId:"1",
            ipAddress:"127.0.0.1",
            sourceChannelId:"WEB"
          },
           cif:"",
           firstName:"",
           lastName:"",
           position:"",
           teamName:"",
           createdBy:""
        }
        
      } 

      if (!this.userobj) {
        this.userobj = {
            hostHeaderInfo:{
            requestId:"1",
            ipAddress:"127.0.0.1",
            sourceChannelId:"WEB"
          },
           cif:"",
           createdBy:""
        }
        
      } 

       if (!this.formData) {
        this.formData = {
          accountNumber:''
        }
        
      }

      
  }

  ngOnInit() {

  	var key = "userObj";
    var userInfo = JSON.parse(this.storage_service.getInfo(key));

    if(!userInfo){
      this.router.navigate(['/login']);
    }
    if(userInfo != null){
      this.captainName =userInfo.capName;
      this.teamName = userInfo.teamName;
      this.createdBy = userInfo.cif;

      console.log(this.createdBy);
      }
    this.userobj.createdBy = this.createdBy;  

    // if(this.duser==true){
    //   this.addsign = true;
    // }

    // else if(this.duser4==true){
    //   this.addsign = false;
    // }

  	
  }

  ngAfterViewInit(){
    var self = this;

    if(jQuery.ready){

      jQuery('.accFormatter').on("keypress", function(event){
          if((event.which != 8) || (event.which != 46)){
            var accFirstChar = self.formData.accountNumber.substring(0, 1);
            if((accFirstChar=='9')||(accFirstChar=='0')){

              if(self.formData.accountNumber.length==2){
                var acc1 = self.formData.accountNumber
                self.formData.accountNumber = acc1 + " ";
              
              }
          
              else if(self.formData.accountNumber.length==7){
                var accNum2 = self.formData.accountNumber;
                var acc1 = accNum2.substring(0, 2);
                var acc2 = accNum2.substring(3, 7);
                self.formData.accountNumber = acc1 + " " + acc2 + " ";
                
              }
          
              else if(self.formData.accountNumber.length==11){
                var accNum2 = self.formData.accountNumber;
                  var acc1 = accNum2.substring(0, 2);
                  var acc2 = accNum2.substring(3, 7);
                  var acc3 = accNum2.substring(8, 11);
                  self.formData.accountNumber = acc1 + " " + acc2 + " " + acc3 + " ";
              } 

            }


              
          }
      });
    }
  }

  

  add(){

    jQuery('.modal').modal(
       {
         dismissible: true,
         opacity: .5,
         inDuration: 300,
         outDuration: 200,
         startingTop: '1%',
         endingTop: '10%',
       }
     );
     jQuery('.accountModal').modal('open');

  }

  addName(){
    this.loading = true;
    this.details = false;
    var spaceAcc = this.formData.accountNumber;
    var accNo = spaceAcc.replace(/ /g, "");
    this.formData.accountNumber = accNo;
    var info = JSON.stringify(this.formData);
     this.api_service.acc_info(info).subscribe(res => {
      this.resp = res;
       if (this.resp.hostHeaderInfo.responseCode == "000") {
          this.loading = false;
          this.formCap.firstName = this.resp.customerInfo.firstName;
          this.formCap.lastName = this.resp.customerInfo.lastName;
          this.formCap.cif = this.resp.accountsInfo[0].cif;
          this.formCap.position = "Member";
          this.formCap.teamName = this.teamName;
          this.formCap.createdBy = this.createdBy;


          this.accountName = this.resp.customerInfo.firstName +" " + this.resp.customerInfo.lastName ;
          this.details = true; 
          // console.log(this.resp.accountsInfo[0].cif);
        }
        else{
          this.details = false;
          this.formData.accountNumber = '';
          // console.log(this.resp);
          this.loading = false;
          toast("Invalid Account Number",3000);
        }
      }, error => {
          this.details = false;
          this.formData.accountNumber = '';
          this.loading = false;
            toast('Oops. Please try again later',3000);
      });


  }

  addMember(){

    this.loader = true;
    var info = JSON.stringify(this.formCap);
    this.api_service.afcon_mem(info).subscribe(res => {
      this.response = res;
       if (this.response.hostHeaderInfo.responseCode == "000") {
        this.loader = false;
        jQuery('.accountModal').modal('close');
        this.details = false;
        this.formData.accountNumber = '';

        if(this.duser==false){
          this.duser = true;
          this.user1 = this.resp.customerInfo.firstName; 
          this.user1CIF = this.formCap.cif;
          console.log(this.user1CIF);
          this.addsign = true;
        }
        

        else if ((this.duser==true)&&(this.duser2==false)) {
          this.duser2 = true;
          this.user2 = this.resp.customerInfo.firstName; 
          this.user2CIF = this.formCap.cif;
          this.addsign = true;
        }
        
        else if ((this.duser2==true)&&(this.duser3==false)) {
          this.duser3 = true;
          this.user3 = this.resp.customerInfo.firstName; 
          this.user3CIF = this.formCap.cif;
          this.addsign = true;
        }

        else if ((this.duser3==true)&&(this.duser4==false)) {
          this.duser4 = true;
          this.user4 = this.resp.customerInfo.firstName; 
          this.user4CIF = this.formCap.cif;
          this.addsign = false;
        }
        


        
        
        }
        else{
          this.loader = false;
          toast("Team member already exists",3000);
        }
      }, error => {
          this.loader = false;
            toast('Oops. Please try again later',3000);
      });


    
  }




  account(event: Event){


      let elementId: string = (event.target as Element).id;
      this.modalID = elementId;

      if(this.modalID=="1"){
        this.userobj.cif = this.user1CIF;
        var users = JSON.stringify(this.userobj);

        this.api_service.del_mem(users).subscribe(res => {
          this.response = res;
           if (this.response.hostHeaderInfo.responseCode == "000") {
            toast("Deleted Member Succesfully",3000);
            if(this.duser2==false){
              this.duser = false;
              this.user1 = '';
              this.user1CIF = '';
              this.addsign = false;
            }
            else if((this.duser2==true)&&(this.duser3==false)&&(this.duser4==false)){
              this.duser2 = false;
              this.duser = true;
              this.addsign = true;
              this.user1CIF = this.user2CIF;
              this.user1 = this.user2;
            }
  
            else if((this.duser2==true)&&(this.duser3==true)&&(this.duser4==false)){
              this.duser2 = true;
              this.duser = true;
              this.duser3 = false;
              this.addsign = true;
              this.user1CIF = this.user2CIF;
              this.user1 = this.user2;
              this.user2CIF = this.user3CIF;
              this.user2 = this.user3;
            }
  
            else if((this.duser2==true)&&(this.duser3==true)&&(this.duser4==true)){
              this.duser2 = true;
              this.duser = true;
              this.duser3 = true;
              this.duser4 = false;
              this.addsign = true;
              this.user1CIF = this.user2CIF;
              this.user1 = this.user2;
              this.user2CIF = this.user3CIF;
              this.user2 = this.user3;
              this.user3CIF = this.user4CIF;
              this.user3 = this.user4;
            }
  
              
            }
            else{
              this.loading = false;
              toast("Couldn't delete user",3000);
            }
          }, error => {
              this.loading = false;
              toast('Oops. Please try again later',2000);
          });
    
        

          
      }

      else if(this.modalID=="2"){
        this.userobj.cif = this.user2CIF;
        var users = JSON.stringify(this.userobj);

        this.api_service.del_mem(users).subscribe(res => {
          this.response = res;
           if (this.response.hostHeaderInfo.responseCode == "000") {
            toast("Deleted Member Succesfully",3000);

            if((this.duser==true)&&(this.duser2==true)&&(this.duser3==false)&&(this.duser4==false)){
              this.duser2= false;
              this.user2 = '';
              this.user2CIF = '';
              this.addsign = true;
            }
            else if((this.duser2==true)&&(this.duser3==true)&&(this.duser4==false)){
              this.duser3 = false;
              this.duser2 = true;
              this.addsign = true;
              this.user2CIF = this.user3CIF;
              this.user2 = this.user3;
            }
    
            else if((this.duser2==true)&&(this.duser3==true)&&(this.duser4==true)){
              this.duser2 = true;
              this.duser3 = true;
              this.duser4 = false;
              this.addsign = true;
              this.user2CIF = this.user3CIF;
              this.user2 = this.user3;
              this.user3CIF = this.user4CIF;
              this.user3 = this.user4;
            }
    
              
            }
            else{
              this.loading = false;
              toast("Couldn't delete user",3000);
            }
          }, error => {
              this.loading = false;
              toast('Oops. Please try again later',2000);
          });
        
        

        
      }

      else if(this.modalID=="3"){
        this.userobj.cif = this.user3CIF;
        var users = JSON.stringify(this.userobj);
        
        this.api_service.del_mem(users).subscribe(res => {
          this.response = res;
           if (this.response.hostHeaderInfo.responseCode == "000") {
            toast("Deleted Member Succesfully",3000);
            if((this.duser==true)&&(this.duser2==true)&&(this.duser3==true)&&(this.duser4==false)){
              this.duser3= false;
              this.addsign = true;
              this.user3 = '';
              this.user3CIF = '';
            }
    
            else if((this.duser==true)&&(this.duser2==true)&&(this.duser3==true)&&(this.duser4==true)){
              this.duser3= true;
              this.duser4 = false;
              this.user3CIF = this.user4CIF;
              this.user3 = this.user4;
              this.addsign = true;
              
            }
    
  
              
            }
            else{
              this.loading = false;
              toast("Couldn't delete user",3000);
            }
          }, error => {
              this.loading = false;
              toast('Oops. Please try again later',2000);
          });

       
      }

      else if(this.modalID=="4"){
        this.userobj.cif = this.user4CIF;
        var users = JSON.stringify(this.userobj);
        

        this.api_service.del_mem(users).subscribe(res => {
          this.response = res;
           if (this.response.hostHeaderInfo.responseCode == "000") {
            toast("Deleted Member Succesfully",3000);
            if((this.duser==true)&&(this.duser2==true)&&(this.duser3==true)&&(this.duser4==true)){
              this.duser4= false;
              this.addsign = true;
              this.user4CIF = '';
              this.user4 = '';
            }
  
              
            }
            else{
              this.loading = false;
              toast("Couldn't delete user",3000);
            }
          }, error => {
              this.loading = false;
              toast('Oops. Please try again later',2000);
          });

        

      }


    
  }



  done(){
  	this.router.navigate(['/done']);
  }

  checkData1(){
    var accFirstChar = this.formData.accountNumber.substring(0, 1);
    // alert(this.formData.accountNumber.length);
    if((accFirstChar=='9')||(accFirstChar=='0')){

       if(this.formData.accountNumber.length==13){
        var accNum2 = this.formData.accountNumber;
          var acc1 = accNum2.substring(0, 2);
          var acc2 = accNum2.substring(2, 6);
          var acc3 = accNum2.substring(6, 9);
          var acc4 = accNum2.substring(9, 13);
          this.formData.accountNumber = acc1 + " " + acc2 + " " + acc3 + " "+ acc4;
      }
  

    }

    else{
      this.formData.accountNumber = this.formData.accountNumber;
    }
    
    
  }

}
