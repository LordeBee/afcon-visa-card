import { Component, OnInit } from '@angular/core';
import { toast } from 'angular2-materialize';
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var jQuery:any;

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  createdBy:any;
  resp:any;
  teamName:any;
  members:any;
  captain:any;
  formData:any;
  tempCaptain:any[];
  tempMembers:any[];
  mems3=false;
  mems4=false;
  mems2=false;
  mems1=false;

  constructor(private route: ActivatedRoute, private router: Router, private api_service:ApiServiceService, private storage_service: StorageServiceService) { 
    this.tempMembers = [];

    if (!this.formData) {
      this.formData = {
        staffNo:''
      }
      
    }
  }

  ngOnInit() {


    var key = "infoObj";
    var userInfo = JSON.parse(this.storage_service.getInfo(key));

    jQuery('.modal').modal(
      {
        dismissible: false,
        opacity: .5,
        inDuration: 300,
        outDuration: 200,
        startingTop: '1%',
        endingTop: '10%',
      }
    );
    jQuery('.loaderModal').modal('open');

    if(!userInfo){
      this.router.navigate(['/user']); 
    }
    if(userInfo != null){
      this.createdBy = userInfo.staffNo;

      this.formData.staffNo = this.createdBy;
      var info = JSON.stringify(this.formData);


      this.api_service.get_members(info).subscribe(res => {
        this.resp = res;
         if (this.resp.hostHeaderInfo.responseCode == "000") {
          jQuery('.loaderModal').modal('close');
           this.teamName = this.resp.teamName;
           var count = this.resp.teamUsers.length;
           for (var i = 0; i < count; i++){
            if (this.resp.teamUsers[i].position == 'Member') {
              this.tempMembers.push(this.resp.teamUsers[i]);
              var mems = this.resp.teamUsers.length;
              if(mems==4){
                this.mems3 = true;
              }

              else if(mems==5){
                this.mems4 = true;
              }

              else if(mems==3){
                this.mems2 = true;
              }
              else if(mems==2){
                this.mems1 = true;
              }
            }

            else if (this.resp.teamUsers[i].position == 'Captain') {
              this.captain = this.resp.teamUsers[i].firstName;

            }
           }
       
         }
         else{
          jQuery('.loaderModal').modal('close');
          toast('Could not load data at this time',3000);
         }
       }, error => {
        jQuery('.loaderModal').modal('close');
                      
               toast('Oops. Please try again later',3000);
         });
      }
  }

  signout(){
    var key = 'infoObj';
    this.storage_service.clearInfo(key);
  	this.router.navigate(['/user']);

  }

  

}
