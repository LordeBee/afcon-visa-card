import { Component, OnInit } from '@angular/core';
import { toast } from 'angular2-materialize'; 
import { ApiServiceService } from '../service/api-service.service';
import { StorageServiceService } from '../service/storage-service.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var jQuery:any;

@Component({
  selector: 'app-done',
  templateUrl: './done.component.html',
  styleUrls: ['./done.component.css']
})
export class DoneComponent implements OnInit {
  captainName:any;
  teamName:any;
  createdBy:any;
  loader=false;
  resp:any;
  tempMembers:any[];
  mems3=false;
  mems4=false;
  mems2=false;
  mems1=false;
  formData:any;

  constructor(private route: ActivatedRoute, private router: Router, private api_service:ApiServiceService, private storage_service: StorageServiceService) { 
    this.tempMembers = [];

    if (!this.formData) {
      this.formData = {
        staffNo:''
      }
      
    }
  }

  ngOnInit() {


    var key = "userObj";
    var userInfo = JSON.parse(this.storage_service.getInfo(key));

    if(!userInfo){
      this.router.navigate(['/login']);
    }
    if(userInfo != null){
      this.captainName =userInfo.capName;
      this.teamName = userInfo.teamName;
      this.createdBy = userInfo.cif;
      console

      }

      jQuery('.modal').modal(
        {
          dismissible: false,
          opacity: .5,
          inDuration: 300,
          outDuration: 200,
          startingTop: '1%',
          endingTop: '10%',
        }
      );
      jQuery('.loaderModal').modal('open');
      
   this.formData.staffNo = this.createdBy;
   var info = JSON.stringify(this.formData);
   this.api_service.get_members(info).subscribe(res => {

     this.resp = res;


     console.log(this.resp);

      if (this.resp.hostHeaderInfo.responseCode == "000") {
        jQuery('.loaderModal').modal('close');
        var count = this.resp.teamUsers.length;
        for (var i = 0; i < count; i++){
         if (this.resp.teamUsers[i].position == 'Member') {
          this.tempMembers.push(this.resp.teamUsers[i]);
           var mems = this.resp.teamUsers.length;
           if(mems==4){
             this.mems3 = true;
           }

           else if(mems==5){
            this.mems4 = true;
          }

          else if(mems==3){
            this.mems2 = true;
          }
          else if(mems==2){
            this.mems1 = true;
          }
           
         }

        }
      }
      else{
        jQuery('.loaderModal').modal('close');
        console.log(this.resp);
      }
    }, error => {
        jQuery('.loaderModal').modal('close');
            console.log(error);
            toast('Oops. Please try again later',3000);
      });


  }

  done(){
    jQuery('.modal').modal(
      {
        dismissible: true,
        opacity: .5,
        inDuration: 300,
        outDuration: 200,
        startingTop: '1%',
        endingTop: '10%',
      }
    );
    jQuery('.accountModal').modal('open');
  }

  exit(){
    var key = 'userObj';
    this.storage_service.clearInfo(key);
    jQuery('.accountModal').modal('close');
  	this.router.navigate(['/login']);

  }

}
