import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {

  private baseUrl =  "http://ghuatgodigisrv1.gh.sbicdirectory.com:8082/";
  // private baseUrl = '/api/';

  private accinfoUrl = this.baseUrl + "AFCON/account_info";
  private addMemberUrl = this.baseUrl + "AFCON/afcon_mem";
  private addTeamUrl = this.baseUrl + "AFCON/afcon_team";
  private delMemberUrl = this.baseUrl + "AFCON/del_mem";
  private getMembersUrl = this.baseUrl + "AFCON/get_members";
  

  private httpHeaders = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Access-Control-Allow-Origin', '*')
  .set("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT")
  .set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization")
  .set('sourceCode','AFCON')
  .set('countryCode','GH');
 
  private options = {
   headers: this.httpHeaders
 };

  constructor(private _http:HttpClient) { }

  

 acc_info(data:any){
    return this._http.post(this.accinfoUrl,data ,this.options); 
  }

  afcon_mem(data:any){
    return this._http.post(this.addMemberUrl,data ,this.options); 
  }

  afcon_team(data:any){
    return this._http.post(this.addTeamUrl,data ,this.options); 
  }

  del_mem(data:any){
    return this._http.post(this.delMemberUrl,data ,this.options); 
  }
  get_members(data:any){
    return this._http.post(this.getMembersUrl,data ,this.options); 
  }


 
}
